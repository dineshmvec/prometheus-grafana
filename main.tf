resource "null_resource" "install_minikube" {
  provisioner "local-exec" {
    command = "winget install minikube -e"
  }
}

resource "null_resource" "start_minikube" { # useful for executing the commands locally
  provisioner "local-exec" {
    command = "minikube start --driver=docker"
  }
   depends_on = [null_resource.install_minikube]
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

resource "helm_release" "prometheus" {
  name       = "prometheus"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  chart      = "prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  values = [
    file("${path.module}/prometheus-values.yaml")
  ]
}


resource "helm_release" "grafana" {
  name       = "grafana"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  chart      = "grafana"
  repository = "https://grafana.github.io/helm-charts"
  values = [
    file("${path.module}/grafana-values.yaml")
  ]
}

resource "kubernetes_deployment" "ping_pong_api" {
  metadata {
    name      = "ping-pong-api"
    namespace = kubernetes_namespace.pingpong.metadata[0].name
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "ping-pong-api"
      }
    }

    template {
      metadata {
        labels = {
          app = "ping-pong-api"
        }
      }

      spec {
        container {
          name  = "ping-pong-api"
          image = "dineshmanii/ping-pong-api"
          port {
            container_port = 8080
          }

          # Add readiness probes
          readiness_probe {
            http_get {
              path = "/health"
              port = 8080
            }
            initial_delay_seconds = 5
            period_seconds        = 10
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "ping_pong_api" {
  metadata {
    name      = "ping-pong-api"
    namespace = kubernetes_namespace.pingpong.metadata[0].name
  }

  spec {
    selector = {
      app = "ping-pong-api"
    }

    port {
      port        = 8080
      target_port = 8080
    }
  }
}

# To scrape the metrics, creating a cluster roles called prometheus scrape
resource "kubernetes_cluster_role" "prometheus_scrape" {
  metadata {
    name = "prometheus-scrape"
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["*"]
  }
}

# Binding the roles to a service account in monitoring namespace to access resources in pingpong namespace
resource "kubernetes_role_binding" "prometheus_scrape_binding" {
  metadata {
    name      = "prometheus-scrape-binding"
    namespace = kubernetes_namespace.pingpong.metadata[0].name
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.prometheus_scrape.metadata[0].name
  }

  subject {
    kind      = "ServiceAccount"
    name      = "prometheus-server"
    namespace = kubernetes_namespace.monitoring.metadata[0].name
  }
}

# creating a namespace for ping-pong application
resource "kubernetes_namespace" "pingpong" {
  metadata {
    name = "pingpong"
  }
}

# Storage class for dynamically creating disk
resource "kubernetes_storage_class" "standard" {
  metadata {
    name = "standard"
  }
  storage_provisioner = "kubernetes.io/gce-pd"
  reclaim_policy      = "Retain"
  parameters = {
    type = "pd-standard"
  }
}

# creating a claim that can be used as part of deployment
resource "kubernetes_persistent_volume_claim" "my_pvc" {
  metadata {
    name = "my-pvc"
  }
  spec {
    access_modes = ["ReadWriteMany"]
    resources {
      requests = {
        storage = "10Gi"
      }
    }
    storage_class_name = kubernetes_storage_class.standard.metadata[0].name
  }
}
