Monitoring Infrastructure and Application with Prometheus and Grafana
=====================================================================
Overview
This document provides an overview of how to set up monitoring for your application using Prometheus and Grafana deployed via Helm through Terraform. It covers the deployment of Prometheus and Grafana, configuration to scrape metrics from your application, and visualization of these metrics on Grafana dashboards.

1. Prerequisites
=================

Before starting, ensure you have:
Terraform installed locally.
Basic understanding of Prometheus, Grafana, Helm, and Kubernetes concepts.

2. Deployment Architecture
==========================

The monitoring setup includes:

Prometheus: Responsible for scraping metrics from monitored targets. It stores time series data and provides querying capabilities.
Grafana: A visualization tool used to create and display dashboards based on data queried from Prometheus.
Ping-Pong Application: A sample application deployed in Kubernetes, generating metrics that Prometheus scrapes and Grafana visualizes.

3. Deployment Steps
===================

Prometheus is deployed using Helm via terraform, a package manager for Kubernetes. Helm charts simplify the deployment and management of Kubernetes applications.

Prometheus needs to be configured to scrape metrics from your Ping-Pong application at pod level. This involves editing a configuration file (prometheus-values.yaml) and specifying scrape targets.
- job_name: ping_pong_api
      kubernetes_sd_configs:
      - role: endpoints
      relabel_configs:
      - action: keep
        regex: ping-pong-api
        source_labels: [__meta_kubernetes_pod_label_app]

Setting up Grafana

Grafana  is deployed using Helm  via terraform

Deployed the ping pong application using the terraform, the application image is built and it is placed in my docker hub which
you are retrieve using docker pull dineshmanii/ping-pong-api

5. Setting up Grafana Dashboards
================================
Connect Grafana to Prometheus as a data source:

Open Grafana UI  and login with admin credentials.
Navigate to Configuration > Data Sources > Add data source.
Select Prometheus and configure endpoint (http://prometheus-server:80).
Create Grafana Dashboards:

Import or create dashboards in Grafana to visualize metrics from the Ping-Pong application.
Use PromQL queries to fetch and display metrics like requests per second, latency, etc.
6. Conclusion
You have successfully deployed Prometheus and Grafana using Helm via Terraform, configured Prometheus to scrape metrics from the Ping-Pong application, and set up Grafana dashboards to visualize these metrics. You can now monitor and analyze the performance of your application in real-time.

This documentation outlines the entire process from setting up the monitoring infrastructure to visualizing application metrics in Grafana. Make sure to customize it further based on your specific deployment details and client requirements.


Commands Used
==============

kubectl exec -it --namespace prometheus-namespace prometheus-deployment-5db9458468-g4fgl -- /bin/sh

minikube service prometheus-server --namespace monitoring

minikube service grafana --namespace monitoring

kubectl port-forward svc/grafana 3000:3000 -n monitoring

kubectl port-forward svc/prometheus-server 9090:9090 -n monitoring

kubectl port-forward svc/ping-pong-api 8080:8080 -n monitoring

kubectl expose service grafana --type=NodePort --target-port=3000 --name=graf-ext -n monitoring

kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=p-ext -n monitoring

kubectl expose service ping-pong-api --type=NodePort --target-port=8080 --name=ping -n monitoring

to check the clusterrolebindings associated with serviceaccount
===============================================================
kubectl get clusterrolebindings -o json | ConvertFrom-Json | `ForEach-Object { $_.items } | ` Where-Object { $_.subjects.name -contains $serviceAccountName }


Before starting
==============
Add the scrape config in yaml
kubectl edit configmap prometheus-server -n monitoring

- job_name: ping_pong_api
      kubernetes_sd_configs:
      - role: endpoints
      relabel_configs:
      - action: keep
        regex: ping-pong-api
        source_labels: [__meta_kubernetes_pod_label_app]

Terraform commands
==================
terraform init
terraform fmt
terraform plan
terraform apply -auto-approve


To start the prometheus and grafana and app
=============================================
Start-Process -NoNewWindow -FilePath "minikube" -ArgumentList "service prometheus-server --namespace monitoring" -PassThru

Start-Process -NoNewWindow -FilePath "minikube" -ArgumentList "service grafana --namespace monitoring" -PassThru

Start-Process -NoNewWindow -FilePath "minikube" -ArgumentList "service ping-pong-api --namespace pingpong" -PassThru
Start-Process -NoNewWindow -FilePath "kubectl" -ArgumentList "port-forward svc/ping-pong-api 8080:8080 -n pingpong"

Service monitor
===============
Define how and what to monitor
we can define which service to monitor
Allows you to define and scrape metrics from services based on various criteria
the prometheus can scrap the metrics from the defined service

prometheus operator
==================
It watches for the service monitor resources and updates the Prometheus configuration accordingly

Service
=======
it is for exposing the deployment

CRD
====
Custom resource definition

For example: service monitor crd is used to monitor the services running in kubernetes and allows prometheus to scrap metrics
from thoses services
=================================================================================================================================
1. Deployed prometheus and grafana via helm chart using terraform

Deployed as Stateful set
alertmanager 
prometheus-prometheus-kube-prometheus-prometheus-0 

Deployed as Daemon
Node exporter

Deployed as Deployment
prometheus operator
kube state metrics

====================================================================================================================================

For Prometheus
https://www.youtube.com/watch?v=dk2-_DbWb80
https://www.youtube.com/watch?v=EeiYpnBHnhY

https://devo.hashnode.dev/comprehensive-aws-eks-cluster-monitoring-with-prometheus-grafanaand-efk-stack-10weeksofcloudops

Helm commands
=============
Helm also uses the ~/.kube/config files for connecting to the cluster

To add the repo to our local (it like a playstore)
Helm repo add https://prometheus-community.github.io/helm-charts

To search a chart repo inside the added repositories (it is like a apps inside the playstore)
Helm search repo prometheus

To install the specified chart based on the (~/.kube/config) to connect to the cluster
Helm install prometheus prometheus-community/prometheus


Steps:
=====
1. Created a Prometheus and grafana via helm using terraform
2. Created a ping-pong-api which expose metrics at /metrics and deployed the docker image at docker hub(dineshmanii)
3. Created a deployment for the ping-pong-api(dineshmanii/ping-pong-api) with the replica set of 3
4. Created a service to expose the deployment, used default cluster IP(within the cluster)
5. Used kubectl expose svc/ping-pong-api --type=NodePort --target-port=8080 --name=ping-ext -n <namespace if needed> to expose the service to node port
6. Accessed the application using port forwarding kuebctl port-forward svc/ping-ext 8080:8080 -n <namespace if needed>
7. For the Prometheus to scrape the metrics, the app deployed in the pod must expose the metrics at a specific endpoint called /metrics
8. Scraped the metrics and showed in prometheus UI


Know issues:
============
Since we are using --driver=docker for the minikube, we are unable to connect using the nodeport IP and port of it, so we are using
the port-forward command, for the accessing the nodeport type service we need to do a additional network config in docker desktop

The Kubectl port-forward command establishes the tunnel from our local port to port on the pod

To do
=====
automate the configmap for prometheus file 
check in grafana
deploy the ping-pong-api in different namespace and give the prometheus service account the necessary permissions and check if the 
metrics are scraped and shown in the prometheus UI --> targets --> service discovery, wait for sometime and check

check by using the cluster IP in garafana datasource url(Prometheus)
====================================================================
kubectl get svc -n monitoring
check the Cluster IP and Port column, example http://10.96.62.17:80


check kubestate metrics


check in prometheus UI
=======================
{job="ping_pong_api"}

